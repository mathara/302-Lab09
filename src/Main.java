import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collectors;
import java.util.Random;

import base.*;
import base.cartas.*;
import base.cartas.magia.*;
import base.controle.Controle;
import base.exception.MesaNulaException;
import base.exception.ValorInvalidoException;
import base.service.impl.ProcessadorServiceImpl;
import util.Util;


public class Main {
	public static void main(String[] args) throws MesaNulaException, ValorInvalidoException {
		Controle control = new Controle();
		
		control.executa();
	}
	

}
