package base;

import java.util.ArrayList;
import base.cartas.Carta;
import util.Util;

public class Mesa {
	private ArrayList<Carta> maoP;
	private ArrayList<Carta> maoS;
	private ArrayList<Carta> lacaiosP;
	private ArrayList<Carta> lacaiosS;
	private int poderHeroiP;
	private int poderHeroiS;
	private int manaP;
	private int manaS;
	
	public Mesa(){
		this.maoP = new  ArrayList<Carta>();
		this.maoS = new  ArrayList<Carta>();
		this.lacaiosP = new  ArrayList<Carta>();
		this.lacaiosS = new  ArrayList<Carta>();
		this.poderHeroiP = Util.PODER_HEROI;
		this.poderHeroiS = Util.PODER_HEROI;
		this.manaP = 1;
		this.manaS = 1;
	}
	
	public Mesa(ArrayList<Carta> maoP, ArrayList<Carta> maoS, ArrayList<Carta> lacaiosP, ArrayList<Carta> lacaiosS,
			int poderHeroiP, int poderHeroiS, int manaP, int manaS){
		this.maoP = maoP;
		this.maoS = maoS;
		this.lacaiosP = lacaiosP;
		this.lacaiosS = lacaiosS;
		this.poderHeroiP = poderHeroiP;
		this.poderHeroiS = poderHeroiS;
		this.manaP = manaP;
		this.manaS = manaS;
	}
	
	public ArrayList<Carta> getMaoP(){
		return maoP;
	}
	
	public void setMaoP(ArrayList<Carta> maoP){
		this.maoP = maoP;
	}
	
	public void addMaoP(Carta maoP){
		(this.maoP).add(maoP);
	}
	
	public ArrayList<Carta> getMaoS(){
		return maoS;
	}
	
	public void setMaoS(ArrayList<Carta> maoS){
		this.maoS = maoS;
	}
	
	public void addMaoS(Carta maoS){
		(this.maoS).add(maoS);
	}
	
	public ArrayList<Carta> getLacaiosP(){
		return lacaiosP;
	}
	
	public void setLacaioP( ArrayList<Carta> lacaiosP){
		this.lacaiosP = lacaiosP;
	}
	
	public ArrayList<Carta> getLacaiosS(){
		return lacaiosS;
	}
	
	public void setLacaioS( ArrayList<Carta> lacaiosS){
		this.lacaiosS = lacaiosS;
	}
	
	public int getPoderHeroiP(){
		return poderHeroiP;
	}
	
	public void setHeroiP(int poderHeroiP){
		this.poderHeroiP = poderHeroiP;
	}
	
	public int getPoderHeroiS(){
		return poderHeroiS;
	}
	
	public void setHeroiS(int poderHeroiS){
		this.poderHeroiS = poderHeroiS;
	}
	
	public int getManaP(){
		return manaP;
	}
	
	public void setManaP(int manaP){
		this.manaP = manaP;
	}
	
	public void decPoderHeroi(int poderHeroi, char autor){
		if (autor == 'P'){
			this.poderHeroiP -= poderHeroi;
		}
		else if (autor == 'S'){
			this.poderHeroiS -= poderHeroi;
		}
	}
	
	public int getManaS(){
		return manaS;
	}
	
	public void setManaS(int manaS){
		this.manaS = manaS;
	}
	
    public void decMana(int mana, char autor){
    	if (autor == 'P'){
			this.manaP -= mana;
		}
    	else if (autor == 'S'){
			this.manaS -= mana;
		}
	}
    
    public Carta sacarCarta(char autor){
    	Carta retirada = null;
    	
    	if (autor == 'P'){
			retirada = maoP.remove(0);
		}
		else if (autor == 'S'){
			retirada = maoS.remove(0);
		}
    	
    	return retirada;
    }
    
    public int getMana(char autor){
    	
    	if (autor == 'P'){
			return getManaP();
		}
		else if (autor == 'S'){
			return getManaS();
		}
    	
    	//caso não escolha um dos jogadores corretos
    	return -1;
    	
	}
}
