package base.cartas.magia;

import java.util.ArrayList;
import java.util.UUID;

import base.cartas.Carta;
import base.cartas.Lacaio;

public class Dano extends Magia{
	private int dano;
	
	public Dano(UUID id ,String nome, int custoMana, int dano) {
		super(id, nome, custoMana);
		this.dano = dano;
	}
	
	public Dano(String nome, int custoMana, int dano) {
		super(nome, custoMana);
		this.dano = dano;
	}
	
	public int getDano(){
		return dano;
	}
	
	public void setDano(int dano){
		this.dano = dano;
	}
	
	@Override
	public String toString(){
		String out = super.toString();
		out += "Dano = " +getDano() + "\n";
		return out;
	}
	
	@Override
	public void usar(Carta alvo){
		Lacaio aux = (Lacaio) alvo;
		aux.setVidaAtual(aux.getVidaAtual() - this.dano);
		
		alvo = aux;
	}
	
}
