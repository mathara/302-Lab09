package base.cartas.magia;

import java.util.UUID;

import base.cartas.Carta;

public abstract class Magia extends Carta{

	public Magia(UUID id, String nome, int custoMana) {
		super(id, nome, custoMana);
	}
	
	public Magia(String nome, int custoMana) {
		super(nome, custoMana);
	}
	
	@Override
	public String toString(){
		String out = super.toString();
		return out;
	}
	

}
