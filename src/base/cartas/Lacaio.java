package base.cartas;

import java.util.UUID;

public class Lacaio extends Carta{
	private int ataque;
	private int vidaAtual;
	private int vidaMaxima;
	private HabilidadesLacaio  habilidade;
	
	//metodo construtor 
	public Lacaio(UUID id, String nome, int custoMana, int ataque, int vidaAtual, int vidaMax){
		super(id, nome, custoMana);
		this.ataque = ataque;
		this.vidaAtual = vidaAtual;
		this.vidaMaxima = vidaMax;  
	}
	
	//construtor reduzido
	public Lacaio(String nome, int custoMana, int ataque, int vidaAtual, int vidaMax /*HabilidadeLacaio*/){
		super(nome, custoMana);
		this.ataque = ataque;
		this.vidaAtual = vidaAtual;
		this.vidaMaxima = vidaMax;  
	}
	
	public Lacaio(String nome, int custoMana, int ataque, int vidaAtual, int vidaMax, HabilidadesLacaio habilidade){
		super(nome, custoMana);
		this.ataque = ataque;
		this.vidaAtual = vidaAtual;
		this.vidaMaxima = vidaMax;
		this.habilidade = habilidade;
	}
	
	
	//Demais metodos
	public int getAtaque(){
		return ataque;
	}
	
	public void setAtaque(int ataque){
		this.ataque = ataque;
	}
	
	public int getVidaAtual(){
		return vidaAtual;
	}
	
	public void setVidaAtual(int vidaAtual){
		this.vidaAtual = vidaAtual;
	}
	
	public int getVidaMaxima(){
		return vidaMaxima;
	}
	
	public void setVidaMaxima(int vidaMaxima){
		this.vidaMaxima = vidaMaxima;
	}
	
	public HabilidadesLacaio getHabilidade(){
		return habilidade;
	}
	
	public void setHabilidade (HabilidadesLacaio h){
		this.habilidade = h;
	}
	
	@Override
	//Alterar
	public String toString(){
		String out = super.toString();
		out += "Ataque = " +getAtaque() + "\n";
		out += "Vida Atual = " +getVidaAtual() + "\n";
		out += "Vida Maxima = " +getVidaMaxima() + "\n";
		return out;
	}
	
	@Override
	public void usar(Carta alvo){
		Lacaio aux = (Lacaio) alvo;
		aux.vidaAtual -= this.ataque;
		alvo = aux;
	}

}
