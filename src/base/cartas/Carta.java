package base.cartas;

import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;

public abstract class Carta {
	private UUID id;
	private String nome;
	private int custoMana;
	
	public Carta (UUID id, String nome,int custoMana){
		this.id =  id;
		this.nome = nome;
		this.custoMana = custoMana;
	}
	
	public Carta(String nome, int custoMana){
		this.id =  UUID.randomUUID();
		this.nome = nome;
		this.custoMana = custoMana;
	}
	
	public UUID getId(){
		return id;
	}
	
	public String getNome(){
		return nome;
	}
	
	public void setNome(String nome){
		this.nome = nome;
	}
	
	public int getCustoMana(){
		return custoMana;
	}
	
	public void setCustoMana(int custoMana){
		this.custoMana = custoMana;
	}
	
	public void usar(Carta alvo){}
	
	public void usar(ArrayList<Carta> alvos) {}
	
	@Override
	public String toString(){
		String out = getNome()+ "(ID:" + getId()+")\n";
		out += "CustoMana:" + getCustoMana()+"\n";
		return out;
		}
	
	@Override
	public boolean equals(Object obj){
		if (this == obj) return true;
		if (!(obj instanceof Carta)) return false;
		Carta card = (Carta) obj;
		if (nome == null || card.nome == null) return false;
		return id == card.id && nome.equals(card.nome);
	}
	
	@Override
	public int hashCode( ){
		int hash = 7;
		
		hash = 47 * hash + Objects.hashCode(id);
		hash = 33 * hash + (nome != null ? nome.hashCode() : 0);
		
		return hash;
	}

}
