package base;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import base.cartas.Carta;
import base.cartas.TipoCarta;
import util.*;


public class Baralho {
	private ArrayList<Carta> vetorCartas;
	
	public Baralho(){
		vetorCartas = new ArrayList<Carta>();
	}
	
	public void adicionarCarta (Carta card){
		if (vetorCartas.size() <  Util.MAX_CARDS){
			vetorCartas.add(card);
			// não preciso de uma váriavel que contenha o tamanho
		}
	}
	
	public void add(List<Carta> card){
		vetorCartas.addAll(card);
	}
	
	public Carta comprarCarta(){
		Carta aux = vetorCartas.get(vetorCartas.size()-1);	//pega o lacaio do topo da pilha
		vetorCartas.remove(vetorCartas.size()-1);					//remove da pilha
		return aux;													//Lacaio do topo da pilha
	}
	
	public void embaralhar(){
		Collections.shuffle(vetorCartas);
		ArrayList<Carta> baux = vetorCartas;
		Collections.reverse(baux);
		System.out.println(baux);
	}
	
	public void imprimirBaralho(){
		System.out.println(vetorCartas);
	}
	
	@Override
	public boolean equals(Object obj){
		return ((this.vetorCartas.equals(((Baralho)obj).vetorCartas))?  true : false);
	}
	
	@Override
	public int hashCode( ){
		int hash = 3;
		return 67 * hash + Objects.hashCode(vetorCartas);
	}
	
//	public void preencherAleatorio(Random generator, int tamanho, int maxMana, int maxAtaque,
//			int maxVida){
//		int min = 0;
//		
//		min = ((Util.MAX_CARDS < tamanho) ? min + Util.MAX_CARDS  : min + tamanho);
//		
//		for (int i = 0; i < min; i++){
//			this.vetorCartas.add(Util.geraCartaAleatoria(generator, maxMana, maxAtaque, maxVida, null));
//		}
//	}
}
