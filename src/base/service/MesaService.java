package base.service;

import java.util.Random;

import base.Baralho;
import base.Mesa;
import base.cartas.TipoCarta;
import base.exception.MesaNulaException;
import base.exception.ValorInvalidoException;

public interface MesaService {
	Mesa adicionaLacaios(Mesa mesa, Random generator, TipoCarta tc) throws MesaNulaException;
	Mesa addMaoInicial(Mesa mesa, Baralho deckJogP, Baralho deckJogS, int primeiro ) throws ValorInvalidoException;
}
