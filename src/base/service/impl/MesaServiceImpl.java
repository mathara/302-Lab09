package base.service.impl;

import static util.Util.MAO_INI;

import java.util.Random;

import base.Baralho;
import base.Mesa;
import base.cartas.TipoCarta;
import base.exception.BaralhoVazioException;
import base.exception.MesaNulaException;
import base.exception.ValorInvalidoException;
import base.service.CartaService;
import base.service.MesaService;
import util.Util;

public class MesaServiceImpl implements MesaService{
	CartaService caux = new CartaServiceImpl();
	
	@Override
	public Mesa adicionaLacaios(Mesa mesa, Random gerador, TipoCarta tc) throws MesaNulaException {
		
		if ( mesa == null){
			throw new MesaNulaException("mesa nula ");
		}
		else{
			for(int i = 0; i< Util.MAX_LACAIOS; i++) {
				mesa.getLacaiosP()
				.add(caux.geraCartaAleatoria(gerador, Util.MAX_MANA, Util.MAX_ATAQUE, Util.MAX_VIDA, TipoCarta.LACAIO));
				
				mesa.getLacaiosS()
					.add(caux.geraCartaAleatoria(gerador, Util.MAX_MANA, Util.MAX_ATAQUE, Util.MAX_VIDA, TipoCarta.LACAIO));
			}
			
		}
		
		return mesa;
	}

	@Override
	public Mesa addMaoInicial(Mesa mesa, Baralho deckJogP, Baralho deckJogS, int mao_ini) throws ValorInvalidoException {
		if (mao_ini < 3) {
			throw new ValorInvalidoException( mao_ini );
		}
		
		for(int i =0; i< mao_ini; i++) {
			mesa.getMaoP().add(deckJogP.comprarCarta());
			
			mesa.getMaoS().add(deckJogS.comprarCarta());
			
		}
		
		mesa.getMaoS().add(deckJogS.comprarCarta());
		
		
		return mesa;
	}
	
	
	
	

}
