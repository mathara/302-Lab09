package base.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import base.cartas.Carta;
import base.cartas.TipoCarta;
import base.service.BaralhoService;
import base.service.CartaService;
import util.RandomString;
import util.Util;

import java.util.Random;

import base.cartas.Carta;
import base.cartas.HabilidadesLacaio;
import base.cartas.Lacaio;
import base.cartas.TipoCarta;
import base.cartas.magia.Buff;
import base.cartas.magia.Dano;
import base.cartas.magia.DanoArea;
import base.exception.BaralhoVazioException;

public class BaralhoServiceImpl implements BaralhoService{
	CartaService caux = new CartaServiceImpl();
	
	public BaralhoServiceImpl(){}
	
	@Override
	public List<Carta> preencherAleatorio(Random generator, int tamanho, int maxMana, int maxAtaque, int maxVida) {
		int min = 0;
		ArrayList<Carta> vetorCartas = new ArrayList<Carta>();
		
		try{
			
			min = ((Util.MAX_CARDS < tamanho) ? min + Util.MAX_CARDS  : min + tamanho);
			
			for (int i = 0; i < min; i++){
				vetorCartas.add(caux.geraCartaAleatoria(generator, maxMana, maxAtaque, maxVida, null));
			}
			
			return vetorCartas;
			
		}catch(BaralhoVazioException e ) {
			System.out.println( e.getMessage() ) ;
		}
		
		return vetorCartas;
	}
}
