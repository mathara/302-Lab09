package base.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

import base.Jogada;
import base.Mesa;
import base.cartas.*;
import base.cartas.magia.*;
import base.service.ProcessadorService;

public class ProcessadorServiceImpl implements ProcessadorService {

	public ProcessadorServiceImpl() {
	}

	public boolean processar(Jogada jogada, Mesa mesa) {
		// log da jogada
		System.out.println("Log de jogada:\n");
		System.out.println(jogada);
		if (jogada.getAuthor() == 'P') {
			System.out.println("Quantidade de Lacaios Adv: " + (mesa.getLacaiosS()).size() + "\n" + mesa.getLacaiosS());
			System.out.println("Poder heróico Adv: " + mesa.getPoderHeroiS());
		} else if (jogada.getAuthor() == 'S') {
			System.out.println("Quantidade de Lacaios Adv: " + (mesa.getLacaiosP()).size() + "\n" + mesa.getLacaiosP());
			System.out.println("Poder heróico Adv: " + mesa.getPoderHeroiP());
		}

		// decrementa a manda do autor da jogada
		if (jogada.getAuthor() == 'P') {
			mesa.setManaP(mesa.getManaP() - jogada.getJogada().getCustoMana());
		} else if (jogada.getAuthor() == 'S') {
			mesa.setManaS(mesa.getManaS() - jogada.getJogada().getCustoMana());
		}

		// verificar o tipo de carta
		Carta card = jogada.getJogada();
		if (card instanceof DanoArea) {
			if (jogada.getAuthor() == 'P') {
				((DanoArea) card).usar(mesa.getLacaiosS());
				// vida
				mesa.setHeroiS(mesa.getPoderHeroiS() - ((DanoArea) card).getDano());
			} else if (jogada.getAuthor() == 'S') {
				((DanoArea) card).usar(mesa.getLacaiosP());
				// vida
				mesa.setHeroiP(mesa.getPoderHeroiP() - ((DanoArea) card).getDano());
			}

		} else if (card instanceof Dano) {
			ArrayList<Carta> local = null;
			if (jogada.getAuthor() == 'P') {
				local = mesa.getLacaiosP();
			} else if (jogada.getAuthor() == 'S') {
				local = mesa.getLacaiosS();
			}

			// pega quem tem habilidade provocar
			Carta provocar = (Carta) local.stream()
					.filter(p -> p instanceof Lacaio && ((Lacaio) p).getHabilidade() == HabilidadesLacaio.PROVOCAR)
					.findFirst().orElse(null);

			if (provocar == null) {
				((Dano) card).usar(jogada.getAlvo());
			} else {
				((Dano) card).usar(provocar);
			}

		} else if (card instanceof Buff) {
			Carta cardalvo = jogada.getAlvo();
			if (cardalvo instanceof Lacaio) {
				((Buff) card).usar(cardalvo);
			}
		} else if (card instanceof Lacaio) {
			if (((Lacaio) card).getHabilidade() == HabilidadesLacaio.EXAUSTAO) {
				// adiciona aos lacaios do jogador
				if (jogada.getAuthor() == 'P') {
					mesa.getLacaiosP().add(card);
				} else if (jogada.getAuthor() == 'S') {
					mesa.getLacaiosS().add(card);
				}
			} else if (((Lacaio) card).getHabilidade() == HabilidadesLacaio.INVESTIDA) {
				// permite atacar o outro lacaio atacar diretamente
				// decrementa a vida do alvo

				// pega quem tem habilidade provocar
				Carta provocar = (Carta) mesa.getLacaiosP().stream()
						.filter(p -> p instanceof Lacaio && ((Lacaio) p).getHabilidade() == HabilidadesLacaio.PROVOCAR)
						.findFirst().orElse(null);

				if (provocar == null) {
					// tira vida do alvo
					if (jogada.getAlvo() == null){
						if (jogada.getAuthor() == 'P'){
							mesa.decPoderHeroi( ((Lacaio) card).getAtaque(), 'S' );
						}else{
							mesa.decPoderHeroi( ((Lacaio) card).getAtaque(), 'P' );;
						}
						
						
					}else{
						((Lacaio) jogada.getAlvo()).usar(card);
						// tira vida do card
						((Lacaio) card).usar(jogada.getAlvo());
					}
				} else {
					// tira vida do alvo
					((Lacaio) provocar).usar(card);
					// tira vida do card
					((Lacaio) card).usar(provocar);
				}

			}
		}
		// atualiza os lacaios, retirando lacaios mortos(<0)
		// campo do jogador P
		mesa.setLacaioP((ArrayList<Carta>) mesa.getLacaiosP().stream()
				.filter(p -> p instanceof Lacaio && ((Lacaio) p).getVidaAtual() > 0).collect(Collectors.toList()));

		// campo do jogador S
		mesa.setLacaioS((ArrayList<Carta>) mesa.getLacaiosS().stream()
				.filter(p -> p instanceof Lacaio && ((Lacaio) p).getVidaAtual() > 0).collect(Collectors.toList()));

		// log da jogada
		System.out.println("Log de jogada:\n");
		if (jogada.getAuthor() == 'P') {
			System.out.println("Quantidade de Lacaios Adv: " + (mesa.getLacaiosS()).size() + "\n" + mesa.getLacaiosS());
			System.out.println("Poder heróico Adv: " + mesa.getPoderHeroiS());
		} else if (jogada.getAuthor() == 'S') {
			System.out.println("Quantidade de Lacaios Adv: " + (mesa.getLacaiosP()).size() + "\n" + mesa.getLacaiosP());
			System.out.println("Poder heróico Adv: " + mesa.getPoderHeroiP());
		}

		// verifica se um dos héroís morreu nesse turno
		// true para jogador morto
		if (jogada.getAuthor() == 'P') {
			if (mesa.getPoderHeroiS() > 0) {
				return false;
			}
			return true;
		} else {// jogada.getAuthor() == 'S'
			if (mesa.getPoderHeroiP() > 0) {
				return false;
			}
			return true;
		}
	}
}
