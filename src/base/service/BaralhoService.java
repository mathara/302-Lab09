package base.service;

import java.util.List;
import java.util.Random;

import base.cartas.Carta;

public interface BaralhoService {
	
	List<Carta> preencherAleatorio(Random generator, int tamanho, int maxMana, int maxAtaque,
			int maxVida);
	

}
